package me.tylergrissom.firstsponge;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

/**
 * Copyright (c) 2013-2018 Tyler Grissom
 */
@Plugin(id="firstsponge", name="First Sponge", version="1.0.0")
public class FirstSpongePlugin {

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        Player player = event.getTargetEntity();

        player.sendMessage(Text.of("Welcome!"));
    }
}
